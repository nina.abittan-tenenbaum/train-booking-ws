package com.trains.booking.databases;


import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.trains.booking.datatypes.*;
import com.trains.booking.services.Reservation;

public class ReservationDatabase {

    private final static Map<String, Reservation> reservations = new HashMap<>();

    static {
            createSampleReservation("user1", "StationA", "StationB","2024-01-15","2024-01-16", NumberOfTickets.TWO.getQuantity(), TravelClass.FIRST.getDescription(), Flexibility.FLEXIBLE.getValue(), 200.0);
            createSampleReservation("user2", "StationA", "D", "2024-01-17", "2024-01-18", NumberOfTickets.ONE.getQuantity(), TravelClass.STANDARD.getDescription(), Flexibility.FLEXIBLE.getValue(), 200.0);
            createSampleReservation("user3", "StationE", "StationF", "2024-01-19", "2024-01-20", NumberOfTickets.THREE.getQuantity(), TravelClass.PREMIUM.getDescription(), Flexibility.NON_FLEXIBLE.getValue(), 120.0);
            
    }

    public boolean containsReservation(String reservationId) {
        return reservations.containsKey(reservationId);
    }


	public Reservation getReservation(String reservationId) {
        return reservations.get(reservationId);
    }

    public static boolean createReservation(String userId, String departureStation, String arrivalStation, String departureDate,
                                      String returnDate, int numberOfTickets, String travelClass, boolean flexible,
                                      double totalPrice) {
        // Generate a unique reservation ID
        String reservationId = generateUniqueReservationId();

        // Create a new reservation
        Reservation reservation = new Reservation(reservationId, userId, departureStation, arrivalStation,
                departureDate, returnDate, numberOfTickets, travelClass, flexible, totalPrice);

        // Store the reservation in the database
        reservations.put(reservationId, reservation);

        return true;
    }

    public boolean updateReservation(String reservationId, Reservation updatedReservation) {
        if (containsReservation(reservationId)) {
            reservations.put(reservationId, updatedReservation);
            return true;
        }
        return false;
    }

    public boolean deleteReservation(String reservationId) {
        if (containsReservation(reservationId)) {
            reservations.remove(reservationId);
            return true;
        }
        return false;
    }

    private static String generateUniqueReservationId() {
        return UUID.randomUUID().toString();
    }
    
    private static void createSampleReservation(String userId, String departureStation, String arrivalStation, String departureDate,
			            String returnDate, int numberOfTickets, String travelClass, boolean flexible,
			            double totalPrice) {
				boolean done = createReservation(userId, departureStation,  arrivalStation,  departureDate,
				        returnDate,  numberOfTickets, travelClass,  flexible,
				      totalPrice);
			
    }
   
}
