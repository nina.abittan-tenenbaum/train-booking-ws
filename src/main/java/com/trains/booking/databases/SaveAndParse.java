package com.trains.booking.databases;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class SaveAndParse {
	public static final String DB_PATH =  System.getProperty("user.dir")+"/TrainBookingWS_DB/";
	
    public static String saveToCSV(String filename) {
    	File file = new File(DB_PATH + filename + ".csv");
        File parentDir = file.getParentFile();
        if (parentDir != null && !parentDir.exists()) {
            parentDir.mkdirs(); // creates directories including any necessary but nonexistent parent directories
        }

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            for (Map.Entry<String, String> entry : AuthDataBase.users.entrySet()) {
                writer.write(entry.getKey() + "," + entry.getValue());
                writer.newLine();
            }
            return "Successfully saved the user data to: " + DB_PATH + filename + ".csv";
        } catch (IOException e) {
            e.printStackTrace();
            return "Error saving user data to text file: " + e.getMessage();
        }
    }
    public static String parseFromCSV(String filename) {
        try (BufferedReader reader = new BufferedReader(new FileReader(DB_PATH+filename+".csv"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] fields = line.split(",");
                if (fields.length == 2) {
                    String username = fields[0];
                    String password = fields[1];
                    if (!AuthDataBase.users.containsKey(username)) { // Check if username already exists
                    	AuthDataBase.users.put(username, password);
                    } else {
                        System.out.println("Skipping duplicate username: " + username);
                    }
                } else {
                    System.out.println("Skipping invalid line: " + line);
                }
            }
            return "Successfully parsed the user data from: " + filename + " to internal database(java data structure)";
        } catch (IOException e) {
            e.printStackTrace();
            return "Error parsing user data from CSV file: " + e.getMessage();
        }
    }

   
}
