package com.trains.booking.databases;

import java.util.HashMap;
import java.util.Map;


public class AuthDataBase {

    protected final static Map<String, String> users = new HashMap<String, String>() {{
    	put("user1", "password1");
        put("user2", "password2");
        put("user3", "password3");
    }};
    private static String userId = "";
    

    public boolean containsUser(String username) {
        return users.containsKey(username);
    }

    public boolean validateUser(String username, String password) {
        if (containsUser(username)) {
        	userId = username;
            return users.get(username).equals(password);
        }
        return false;
    }

    public boolean addUser(String username, String password) {
        if (!containsUser(username)) {
            users.put(username, password);
            userId = username;
            return true;
        }
        return false;
    }

    public boolean removeUser(String username) {
        if (containsUser(username)) {
            users.remove(username);
            return true;
        }
        return false;
    }
    
    public String getUserId() {
    	return userId;
    }
    

    public String saveToCSV(String filename) {
    	String result = SaveAndParse.saveToCSV(filename);
    	return result;
    }

    public String parseFromCSV(String filename) {
    	String result = SaveAndParse.parseFromCSV(filename);
    	return result;
    }


}
