package com.trains.booking.services;

import com.trains.booking.databases.AuthDataBase;

public class AuthService {


    public AuthService() {}

    public String login(String username, String password) {
    	AuthDataBase database = new AuthDataBase();
    	if (database.validateUser(username, password)) {
            return database.getUserId();
        }
        return "";
    }

    public boolean register(String username, String password) {
    	AuthDataBase database = new AuthDataBase();
    	boolean hasregistered = database.addUser(username, password);
    	return hasregistered;
    }


}