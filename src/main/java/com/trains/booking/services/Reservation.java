package com.trains.booking.services;


public class Reservation {

    private final String reservationId;
    private final String userId;
    private final String departureStation;
    private final String arrivalStation;
    private final String departureDate;
    private final String returnDate;
    private final int numberOfTickets;
    private final String travelClass;
    private final boolean flexible;
    private final double totalPrice;

    public Reservation(String reservationId, String userId, String departureStation, String arrivalStation,
    		String departureDate, String returnDate, int numberOfTickets, String travelClass,
    		boolean flexible, double totalPrice) {
    	
    	this.reservationId = reservationId;
        this.userId = userId;
        this.departureStation = departureStation;
        this.arrivalStation = arrivalStation;
        this.departureDate = departureDate;
        this.returnDate = returnDate;
        this.numberOfTickets = numberOfTickets;
        this.travelClass = travelClass;
        this.flexible = flexible;
        this.totalPrice = totalPrice;
    }


	@Override
    public String toString() {
        return "Reservation Details:\n" +
                "Reservation ID: " + reservationId + "\n" +
                "User ID: " + userId + "\n" +
                "Departure Station: " + departureStation + "\n" +
                "Arrival Station: " + arrivalStation + "\n" +
                "Departure Date: " + departureDate + "\n" +
                "Return Date: " + returnDate + "\n" +
                "Number of Tickets: " + numberOfTickets + "\n" +
                "Travel Class: " + travelClass + "\n" +
                "Flexible: " + flexible + "\n" +
                "Total Price: " + totalPrice;
    }


	public String getReservationId() {
		return reservationId;
	}


	public String getUserId() {
		return userId;
	}


	public String getDepartureStation() {
		return departureStation;
	}


	public String getArrivalStation() {
		return arrivalStation;
	}


	public String getDepartureDate() {
		return departureDate;
	}


	public String getReturnDate() {
		return returnDate;
	}


	public int getNumberOfTickets() {
		return numberOfTickets;
	}


	public String getTravelClass() {
		return travelClass;
	}


	public boolean isFlexible() {
		return flexible;
	}


	public double getTotalPrice() {
		return totalPrice;
	}



}