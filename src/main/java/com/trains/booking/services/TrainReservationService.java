package com.trains.booking.services;

import com.trains.booking.databases.ReservationDatabase;


public class TrainReservationService {

    private final AuthService authService;

    public TrainReservationService() {
        new ReservationDatabase();
        this.authService = new AuthService();
    }

    public String createReservation(String username, String password, String departureStation, String arrivalStation,
		        String departureDate, String returnDate, int numberOfTickets, String travelClass,
		        boolean flexible, double totalPrice) {
			String userId = authService.login(username, password);
			if (!userId.isEmpty()) {
			    
			    @SuppressWarnings("unused")
				boolean done = ReservationDatabase.createReservation(userId, departureStation, arrivalStation, departureDate,
					returnDate, numberOfTickets, travelClass, flexible, totalPrice);
				return "your reservation has been created";
			} else {
				return "enable to create a reservation for " + userId;
			}
		}


}
