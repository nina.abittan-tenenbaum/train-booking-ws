package com.trains.filtering.filter.database;

import java.sql.*;


public class ConnectDataBase {

	
	public static Connection StartConnection() {
		Connection conn = null;
		try {
			System.out.println("Attempting the database connection...");
			String url = "jdbc:postgresql://localhost:5432/traindata";
			String username = "sncf";
			String password = "sncf";
			conn = DriverManager.getConnection(url,username,password);
		} catch (Exception e) {
			System.err.println("Failed to connect to the database");
			System.err.println(e.toString());
			return null;
		}
		System.out.println("Connection established !");
		return conn;
	}
	
	public static void EndConnection(Connection conn) {
		try {
			if(conn != null || !conn.isClosed()) {
				conn.close();
				System.out.println("Connection closed !");
			}
			
		} catch (Exception e) {
			System.out.println("Error occurred while closing the connection.");
			e.printStackTrace();
		}
	}
}

