package com.trains.filtering.filter;

import java.sql.*;
import java.util.ArrayList;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;
import org.restlet.Response.*;

import com.trains.filtering.filter.database.DBManager;


public class TrainResource extends ServerResource {

	private static DBManager databaseManager;
	private int idTrain;
	private String departureStation;
	private String arrivalStation;
	private String departDate;
	private String arrivalDate;
	private String departureTime;
	private String arrivalTime;



	public TrainResource(int idTrain, String departDate, String arrivalDate, String departureTime, String arrivalTime, String departureStation, String arrivalStation, ArrayList<Seat> someSeatsInfo) {
		this.databaseManager = new DBManager();
		this.idTrain = idTrain;
		this.departDate = departDate;
		this.arrivalDate = arrivalDate;
		this.departureTime = departureTime;
		this.arrivalTime = arrivalTime;
		this.departureStation = departureStation;
		this.arrivalStation = arrivalStation;
	}

	public TrainResource() {
		this.databaseManager = new DBManager();
		this.idTrain = 0;
		this.departDate = "";
		this.arrivalDate = "";
		this.departureTime = "";
		this.arrivalTime = "";
		this.departureStation = "";
		this.arrivalStation = "";
	}

	@Get("txt")
	public String printFilteredTrain() throws Exception {
		int numberOfSeats = 1;

		String departAttribute = getAttribute("depart");
		String arrivalAttribute = getAttribute("arrival");

		String departureTimeAttribute = getQueryValue("departureTime");
		String arrivalTimeAttribute = getQueryValue("arrivalTime");
		String travelClass = getQueryValue("class");


		String seatsParam = getQueryValue("seats");
		if (seatsParam != null && !seatsParam.isEmpty()) {
			try {
				numberOfSeats = Integer.parseInt(seatsParam);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}

		String dayAttribute = getQueryValue("day");
		if (dayAttribute == null || dayAttribute.isEmpty()) {
			dayAttribute = "2023-09-08";
		}

		ArrayList<String> trainsList = getFilteredTrains(departAttribute, arrivalAttribute, dayAttribute, departureTimeAttribute, arrivalTimeAttribute, numberOfSeats, travelClass);

		if(trainsList.isEmpty())
			return "No available train";

		String response = "Information about trains leaving the " + dayAttribute + " from " + departAttribute + " to " + arrivalAttribute + ":\n\n";

		for (String train : trainsList)
			response += train + "\n";

		return response;
	}

	@Post("form:txt")
	public Representation updateSeatsAvailability(Form form) throws SQLException {

		int numOfSeat = Integer.parseInt(form.getFirstValue("numOfSeat"));
		int trainId = Integer.parseInt(form.getFirstValue("trainId"));
		String travelClass = form.getFirstValue("travelClass");

		String updateQuery = "UPDATE Seat SET availableSeats = availableSeats - ? WHERE trainID = ? AND \"class\" = ? ";

		ArrayList<Object> paramsDeparture = new ArrayList<>();
		paramsDeparture.add(numOfSeat);
		paramsDeparture.add(trainId);
		paramsDeparture.add(travelClass);

		try {
			databaseManager.executeUpdate(updateQuery, paramsDeparture);
			return new StringRepresentation("Seats updated successfully");
		} catch (SQLException e) {
			return new StringRepresentation(e.getMessage());
		}
	}

	public static ArrayList<String> getFilteredTrains(
			String aDepart,
			String anArrival,
			String aDay,
			String departureTime,
			String arrivalTime,
			int seats,
			String travelClass
	){
		ArrayList<String> trainsList = new ArrayList<>();
		ArrayList<Integer> trainIdList = new ArrayList<>();
		ArrayList<Object> RequestArguments = new ArrayList<>();
		RequestArguments.add(aDepart);
		RequestArguments.add(anArrival);
		RequestArguments.add(aDay);

		try {
			StringBuilder queryBuilder = new StringBuilder(
					"SELECT Train.trainID, Train.departure_station, Train.arrival_station, " +
							"Train.departure_date, Train.departure_time, " +
							"Seat.class, Seat.availableSeats, " +
							"Pricing.nonflexibleprice, Pricing.flexibleprice " +
							"FROM Train " +
							"JOIN Seat ON Train.trainID = Seat.trainID " +
							"JOIN SeatPricing AS Pricing ON Seat.pricingID = Pricing.pricingID " +
							"WHERE Train.departure_station = ? " +
							"AND Train.arrival_station = ? " +
							"AND Train.departure_date::date = CAST(? AS DATE) "
			);

			if (departureTime != null && !departureTime.isEmpty()) {
				queryBuilder.append("AND Train.departure_time = CAST(? AS TIME) ");
				RequestArguments.add(departureTime);
			}

			if (arrivalTime != null && !arrivalTime.isEmpty()) {
				queryBuilder.append("AND Train.arrival_time = CAST(? AS TIME) ");
				RequestArguments.add(arrivalTime);
			}

			if (seats > 1) {
				queryBuilder.append("AND Seat.availableSeats >= ? ");
				RequestArguments.add(seats);
			}

			if (travelClass != null && !travelClass.isEmpty()) {
				queryBuilder.append("AND Seat.class = ? ");
				RequestArguments.add(travelClass);
			}

			queryBuilder.append("ORDER BY Train.trainID;");

			ResultSet res = databaseManager.executeQuery(queryBuilder.toString(), RequestArguments);

			while (res.next()) {
				int trainID = res.getInt("trainID");
				String departureStation = res.getString("departure_station");
				String arrivalStation = res.getString("arrival_station");
				String departureDate = res.getString("departure_date");
				String departTime = res.getString("departure_time");

				String travelClassRes = res.getString("class");
				int availableSeats = res.getInt("availableSeats");
				int nonFlexiblePrice = res.getInt("nonflexibleprice");
				int flexiblePrice = res.getInt("flexibleprice");

				String trainInfo = "";

				if (!trainIdList.contains(trainID)) {
					trainIdList.add(trainID);
					trainInfo = "\n Train id: " + trainID + ", From " + departureStation + " To " + arrivalStation + ", Departure: " + departureDate + " " + departTime + "\n";
				}

				String seatInfo = "|Seat in " + travelClassRes + " class| Available Seats: " + availableSeats + ", Flexible Price: " + flexiblePrice + ", Non Flexible Price: " + nonFlexiblePrice + "\n";

				String trainDetails = trainInfo + seatInfo;
				trainsList.add(trainDetails);
			}

			if(trainsList.isEmpty())
				throw new Exception("No train available");

		} catch (SQLException e) {
			System.out.println("Unable to fetch data from the database");
			e.printStackTrace();
		}
		catch (Exception e) {
			System.out.println("No available train");
			e.printStackTrace();
		}

		return trainsList;
	}
}