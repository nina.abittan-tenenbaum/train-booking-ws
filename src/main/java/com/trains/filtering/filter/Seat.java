package com.trains.filtering.filter;

public class Seat {

    enum travelClass{
        FIRST,
        BUSSINESS,
        STANDARD
    };

    travelClass aClass;
    int flexiblePrice;
    int nonFlexiblePrice;
    int avalaibleSeats;

    public Seat(travelClass aTravelClass, int aFlexiblePrice, int aNonFlexiblePrice, int aNumberOfAvailableSeats){
        this.aClass = aTravelClass;
        this.flexiblePrice = aFlexiblePrice;
        this.nonFlexiblePrice = aNonFlexiblePrice;
        this.avalaibleSeats = aNumberOfAvailableSeats;
    }

    public static travelClass getTravelClassFromString(String aClassString){
        return switch (aClassString) {
            case "Business" -> travelClass.BUSSINESS;
            case "First" -> travelClass.FIRST;
            case "Standard" -> travelClass.STANDARD;
            default -> travelClass.STANDARD;
        };
    }

}
